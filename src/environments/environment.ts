// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false ,
  
  firebaseConfig: {
    apiKey: "AIzaSyB2aiE83TquxCCtiKKBRLOyG4lMDjsd7zI",
    authDomain: "bookclass-2a9c3.firebaseapp.com",
    databaseURL: "https://bookclass-2a9c3.firebaseio.com",
    projectId: "bookclass-2a9c3",
    storageBucket: "bookclass-2a9c3.appspot.com",
    messagingSenderId: "1034580158274",
    appId: "1:1034580158274:web:b6ca260d5d04151e5a4adb",
    measurementId: "G-LRHS1C6MBQ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
