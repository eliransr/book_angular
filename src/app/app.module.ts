// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

// Angular Material 
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatCardModule} from '@angular/material/card';
import { MatExpansionModule} from '@angular/material/expansion';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatSelectModule} from '@angular/material';
import { MatInputModule} from '@angular/material';

// AngularFire
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule , AngularFirestore} from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule , AngularFireAuth } from '@angular/fire/auth';

// Routing
import { RouterModule, Routes } from '@angular/router';

// HttpClient
import { HttpClientModule } from '@angular/common/http';

// Service
import { PostsService } from './Services/posts.service';

// Components
import { AppComponent } from './app.component';
import { NavComponent } from './Components/nav/nav.component';
import { BooksComponent } from './Components/books/books.component';
import { AuthorsComponent } from './Components/authors/authors.component';
import { EditauthorComponent } from './Components/editauthor/editauthor.component';
import { PostsComponent } from './Components/posts/posts.component';
import { PostformComponent } from './Components/postform/postform.component';
import { SignUpComponent } from './Components/sign-up/sign-up.component';
import { LoginComponent } from './Components/login/login.component';
import { ClassifiedComponent } from './Components/classified/classified.component';
import { DocformComponent } from './Components/docform/docform.component';


const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'authors', component: AuthorsComponent },
  { path: 'authors/:authorname/:id', component: AuthorsComponent },
  { path: 'editauthors/:authorname/:id' ,  component: EditauthorComponent},
  { path: 'posts', component: PostsComponent },
  { path: 'postform', component: PostformComponent},
  { path: 'postform/:id', component: PostformComponent},
  { path: 'signup', component: SignUpComponent},
  { path: 'login', component: LoginComponent},
  { path: 'classify', component: DocformComponent},
  { path: 'classified', component: ClassifiedComponent},
  { path: "",
    redirectTo: '/books',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BooksComponent,
    AuthorsComponent,
    EditauthorComponent,
    PostsComponent,
    PostformComponent,
    SignUpComponent,
    LoginComponent,
    ClassifiedComponent,
    DocformComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatSliderModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    HttpClientModule,

    AngularFireModule.initializeApp(environment.firebaseConfig, 'BooksAngular'),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,

    
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true } // <-- debugging purposes only
    ),
 
    


  ],
  //  providers: [PostsService],
  providers: [AngularFirestore ,
              AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
