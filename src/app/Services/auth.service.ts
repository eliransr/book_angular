import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { User } from '../Interfeces/User';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
 //  Get User or Null
   user: Observable<User | null>
     
   // אם מייצרים אתחול של משתנה , כדאי שזה יהיה בתוך הבנאי
   constructor(public afAuth:AngularFireAuth,
               private router:Router) { 
         this.user = this.afAuth.authState;
   }
 
   getUser(){
     return this.user
   }
 
   signUp(email:string, password:string)
   {
     console.log('In SignUp()')
     console.log("this is the email: ",email)
     console.log("this is the password: ",password);
     this.afAuth.auth.createUserWithEmailAndPassword(email,password).then(
                                                                           user =>
                                                                           {
                                                                             console.log('Succesful sign up')
                                                                             this.router.navigate(['/login']);
                                                                           }
                                                                         )
                                                                         .catch(function(error) 
                                                                                         {
                                                                                           // Handle Errors here.
                                                                                           var errorCode = error.code;
                                                                                           var errorMessage = error.message;
                                                                                           if (errorCode === 'auth/wrong-password') {
                                                                                             alert('Wrong password.');
                                                                                           }
                                                                                           else {
                                                                                             alert(errorMessage);
                                                                                           }
                                                                                           console.log(error);
                                                                                         });                                                                  
 
   }  
   Logout(){
     this.afAuth.auth.signOut();
     console.log('Succesful Logout')
     this.router.navigate(['/books'])
   }
 
 
  Login(email:string, password:string)
   {
     console.log('In Login() in auth.service')
     console.log("the user email: ",email)
     this.afAuth.auth.signInWithEmailAndPassword(email,password).then(
                                                                       (res) => 
                                                                       {
                                                                       //  this.user=res
                                                                       //  console.log("res is: ",res.user)
                                                                       //  this.user.subscribe(
                                                                       //                       user=>
                                                                       //                       {
                                                                       //                         console.log('This is the userID logged in: ',user.uid)
                                                                       //                       }
                                                                       //                     );
                                                                         console.log("Login successfully")
                                                                         this.router.navigate(['/books'])
                                                                       }
                                                                     )
                                                                     .catch(function(error) 
                                                                                         {
                                                                                           // Handle Errors here.
                                                                                           var errorCode = error.code;
                                                                                           var errorMessage = error.message;
                                                                                           if (errorCode === 'auth/wrong-password') {
                                                                                             alert('Wrong password.');
                                                                                           } else {
                                                                                             alert(errorMessage);
                                                                                           }
                                                                                           console.log(error);
                                                                                         });
   
   }
 }