import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {
  
  authors:any = [{id:1, name:'Lewis Carrol'},{id:2, name:'Leo Tolstoy'}, {id:3, name:'Thomas Mann'}]
  i:number = 3;

  getAuthors():any{
    const authorsObservable = new Observable(
      observer => {observer.next(this.authors)
        setInterval(
          () => observer.next(this.authors),100
        )
      }
    )
    return authorsObservable;

  }
  addAuthors(newAuthor:string){
    this.i = this.i+1;
    this.authors.push({id:this.i, name: newAuthor});
  }

  constructor() { }
}

