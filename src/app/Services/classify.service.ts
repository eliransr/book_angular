import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {
  
  private url = "https://og3pek8q79.execute-api.us-east-1.amazonaws.com/beta";
  
  public categories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'}
  
  public doc:string; 
  constructor(private http: HttpClient) { }
  
  classify():Observable<any>{
    console.log(this.doc);
    let json = {
      "articles": [
        {
          "text": this.doc
        },
      ]
    }
    let body  = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        console.log(res.body);
        let final = res.body.replace('[','');
        final = final.replace(']','');
        console.log(final);
        return final;      
      })
    );      
  }

  
}
