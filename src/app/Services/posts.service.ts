import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient ,HttpClientModule } from '@angular/common/http';
import { Post } from '../Interfeces/post';
import { User } from './../Interfeces/User';
import { tap, catchError, map, flatMap } from 'rxjs/operators';
import { AngularFirestore  , AngularFirestoreCollection} from '@angular/fire/firestore';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  // //For read data from API
//  private PostApi = "https://jsonplaceholder.typicode.com/posts" ;
//  private UserApi = "https://jsonplaceholder.typicode.com/users" ; 

  
constructor(private http: HttpClient ,
            private db: AngularFirestore ,
            private authService:AuthService) { }

UserCollection:AngularFirestoreCollection = this.db.collection('Users');
PostCollection:AngularFirestoreCollection;

 // Read
   getPosts(userId:string): Observable<any[]> {
    this.PostCollection = this.db.collection(`Users/${userId}/Posts`);
        console.log('Posts collection created');
        return this.PostCollection.snapshotChanges().pipe(
          map(collection => collection.map(document => {
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            console.log(data);
            return data;
          }))
       );    
    } 
 
    getPost(userId, id:string):Observable<any>{
      return this.db.doc(`Users/${userId}/Posts/${id}`).get();
   }

// Create
    addPost(userId:string , title:string, body:string, author:string ){
      console.log('In add posts');
      const post = {title:title , body:body, author:author };
      this.UserCollection.doc(userId).collection('Posts').add(post);
    }

  // Update
  updatePost(userId:string , id:string ,title:string , body:string ,author:string){
     this.db.doc  (`Users/${userId}/Posts/${id}`).update(  { title:title , body:body, author:author  } )
  }

// Delete
    // Posts = the name of the Collection
    deletePost(userId:string , id:string){
      this.db.doc(`Users/${userId}/Posts/${id}`).delete();
    }


}




// ###############################--Codes --######################################################3###
// // //For get posts from API
// getPosts(): Observable<Post>
//  {
//     return this.http.get<Post>(`${this.PostApi}`);
//   }
//   getUsers(): Observable<User>
//  {
//     return this.http.get<User>(`${this.UserApi}`);
//   }

  // //For Upload posts from API
// getPosts()
// {
//    return this.http.get<Post[]>(`${this.PostApi}`);
//  }
//  getUsers()
// {
//    return this.http.get<User[]>(`${this.UserApi}`);
//  }



// //
// #------C.R.U.D  - Regular------ //

// Read
// getPosts(): Observable<any[]> {
//   const ref = this.db.collection('Posts');
//   return ref.valueChanges({idField: 'id'});
// } 
// getPost(id:string):Observable<any>{
//   return this.db.doc(`Posts/${id}`).get();
// }

 // Update
//  updatePost(id:string ,title:string , body:string ,author:string)
//  {
//      this.db.doc(`Posts/${id}`).update( { title:title , body:body, author:author  } )
//  }

// Delete
  // // Posts = the name of the Collection
  // deletePost(id:string)
  // {
  //   this.db.doc(`Posts/${id}`).delete();
  // }

  // // Create
  // addPost(title:string , body:string, author:string){
  //   const post = { title:title , body:body , author:author};
  //   this.db.collection('Posts').add(post);
  // }


// #############################################################################################################