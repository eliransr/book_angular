import { Component, OnInit } from '@angular/core';
import { BooksService } from 'src/app/Services/books.service';
import { Observable } from 'rxjs/internal/Observable';


@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  // the books list moved to books.services
  //books: object[] = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}, {title:'A Song Of Ice And Fire', author:'George R R Martin'}]

  panelOpenState = false;
  books$:Observable<any>;
  constructor(private booksservice:BooksService) { }

  ngOnInit() {
   this.books$ = this.booksservice.getBooks();
   this.booksservice.addBooks();
  }



}