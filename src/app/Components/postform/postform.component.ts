import { Component, OnInit } from '@angular/core';
import { PostsService } from './../../Services/posts.service';  
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../../Services/auth.service';

@Component({
  selector: 'app-postform',
  templateUrl: './postform.component.html',
  styleUrls: ['./postform.component.css']
})
export class PostformComponent implements OnInit {

  constructor(private postsservice:PostsService, 
              private router:Router,
              private route: ActivatedRoute,
              private authService:AuthService ) { }
    
        title:string;
        body:string;
        author:string; 
        id:string;
        userId:string;
        isEdit:boolean = false;
        buttonText:string = 'Add post' 
        titleText:string ='New post'
        
          
      onSubmit(){ 
        if(this.isEdit){
          console.log('edit mode');
          this.postsservice.updatePost(this.userId , this.id,this.title,this.body, this.author)
        }
         else {
         this.postsservice.addPost(this.userId , this.title,this.body, this.author)
        }
        this.router.navigate(['/posts']);  
      }  
    
    
      ngOnInit() {
        this.id = this.route.snapshot.params.id;
        //  Cheack the user id
          this.authService.getUser().subscribe(
            user => {
            this.userId = user.uid; 
      
            if(this.id) {
            this.isEdit = true;
            this.buttonText = 'Update post'
            this.titleText='Update post'   
            this.postsservice.getPost(this.userId,this.id).subscribe(
              post => {
                console.log(post.data().title)
                console.log(post.data().body)
                console.log(post.data().author)
                this.title = post.data().title; 
                this.body = post.data().body; 
                this.author = post.data().author; })
              }
            })
          }
    }