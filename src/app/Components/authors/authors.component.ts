import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { AuthorsService } from 'src/app/Services/authors.service';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  // authors: object[] = [{id:'1', name:'Lewis Carrol'},{id:'2', name:'Leo Tolstoy'}, {id:'3', name:'Thomas Mann'}]
  // authors:any;
  authors$:Observable<any>;
  author;
  authorname;
  tempid;
  id;
  newAuthor:string;

  constructor(
    private authorservice:AuthorsService,
    private route: ActivatedRoute,
    private router: Router) { }

  // addNewAuthors(){
   
  // }
  ngOnInit() {
    this.authors$ = this.authorservice.getAuthors();
    this.author = this.route.snapshot.params.author;
    this.id = this.route.snapshot.params.id;
    // this.authorservice.addBooks();
  }


  OnSubmit(){
    this.authorservice.addAuthors(this.newAuthor);
  }
}
