import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './../../Services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  
      constructor(private authService:AuthService,
                  private router:Router) { }
    
      email:string;
      password:string; 
    
      onSubmit()
        {
          this.authService.Login(this.email,this.password);
          // this.router.navigate(['/posts']);
        }    
      ngOnInit() 
        {
        }
    
    }